import junit.framework.TestCase;
import org.junit.Test;

public class MyComplexTest extends TestCase {
    MyComplex a = new MyComplex(4,5);
    MyComplex b = new MyComplex(3,7);
    MyComplex c = new MyComplex(2.7,9.5);
    MyComplex d = new MyComplex(8,2.1);
    MyComplex e = new MyComplex(8,15);
    @Test
    public void testgetRealPart() throws Exception {
        assertEquals(4.0,a.getRealPart());
        assertEquals(2.7,c.getRealPart());
    }
    @Test
    public void testgetImagePart()throws Exception{
        assertEquals(7.0,b.getImagePart());
        assertEquals(2.1,d.getImagePart());
    }
    @Test
    public void testComplexjiafa()throws Exception{
        assertEquals(7.0,a.Complexjiafa(b).realpart);
        assertEquals(11.6,c.Complexjiafa(d).imagepart);
    }
    @Test
    public void testComplexjianfa()throws Exception{
        assertEquals(1.0,a.Complexjianfa(b).realpart);
        assertEquals(7.4,c.Complexjianfa(d).imagepart);
    }
    @Test
    public void testComplexchengfa()throws Exception{
        assertEquals(-23.0,a.Complexchengfa(b).realpart);
        assertEquals(81.67,c.Complexchengfa(d).imagepart);
    }
    @Test
    public void testComplexchufa()throws Exception{
        assertEquals(2.0,e.Complexchufa(a).realpart);
        assertEquals(3.0,e.Complexchufa(a).imagepart);
    }
}
