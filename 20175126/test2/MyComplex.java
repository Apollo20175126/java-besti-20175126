import static java.lang.Math.sqrt;

public class MyComplex {
        double realpart,imagepart;
        public MyComplex(double R,double I){
            realpart=R;
            imagepart=I;
        }
        public double getRealPart(){
             return realpart;
        }
        public double getImagePart(){
            return imagepart;
        }
        public MyComplex Complexjiafa(MyComplex a){
            return new MyComplex((realpart+a.realpart),imagepart+a.imagepart);
        }
         public MyComplex Complexjianfa(MyComplex a){
        return new MyComplex((realpart-a.realpart),imagepart-a.imagepart);
         }
         public MyComplex Complexchengfa(MyComplex a){
        return new MyComplex((realpart*a.realpart-imagepart*a.imagepart),realpart*a.imagepart+imagepart*a.realpart);
         }
         public MyComplex Complexchufa(MyComplex a){
            double d = sqrt(a.realpart*a.realpart)+sqrt(a.imagepart*a.imagepart);
            double e = (realpart*a.imagepart-imagepart*a.realpart);
        return new MyComplex((realpart*a.realpart+imagepart*a.imagepart)/d,imagepart/a.imagepart);
    }
        public static void main(String args[]){
            MyComplex w = new MyComplex(2,10);
            MyComplex e = new MyComplex(1,3);
            MyComplex a = new MyComplex(4,5);
            MyComplex b = new MyComplex(3,7);
            a.Complexjiafa(b);
            System.out.println("("+a.realpart+"+"+a.imagepart+"i)+("+b.realpart+"+"+b.imagepart+"i)="+a.Complexjiafa(b).realpart+"+"+a.Complexjiafa(b).imagepart+"i");
            System.out.println("("+a.realpart+"+"+a.imagepart+"i)-("+e.realpart+"+"+e.imagepart+"i)="+a.Complexjianfa(e).realpart+"+"+a.Complexjianfa(e).imagepart+"i");
            System.out.println("("+a.realpart+"+"+a.imagepart+"i)*("+b.realpart+"+"+b.imagepart+"i)="+a.Complexchengfa(b).realpart+"+"+a.Complexchengfa(b).imagepart+"i");
            System.out.println("("+a.realpart+"+"+a.imagepart+"i)/("+w.realpart+"+"+w.imagepart+"i)="+a.Complexchufa(w).realpart+"+"+a.Complexchufa(w).imagepart+"i");
        }

    }

