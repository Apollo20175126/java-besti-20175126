import java.util.Stack;

public class MyDC {
    static Stack<Character> op = new Stack<>();

    public static Float getv(char op, Float f1, Float f2) {
        if (op == '+') {
            return f2 + f1;
        } else if (op == '-') {
            return f2 - f1;
        } else if (op == '*') {
            return f2 * f1;
        } else if (op == '/') {
            return f2 / f1;
        } else {
            return Float.valueOf(-0);
        }
    }

    public static float calrp(String rp) {
        Stack<Float> v = new Stack<>();
        char[] arr = rp.toCharArray();
        int len = arr.length;
        for (int i = 0; i < len; i++) {
            Character ch = arr[i];
            if (ch >= '0' && ch <= '9') {
                v.push(Float.valueOf(ch - '0'));
            } else {
                v.push(getv(ch, v.pop(), v.pop()));
            }
        }
        return v.pop();
    }
}

