import java.time.temporal.JulianFields;
import java.util.Random;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Random rand = new Random();
		Rational r = new Rational();
		Rational answer = new Rational();
		String s = new String();
		int a,b,c,n,yes=0;
		char [] ch = {'+','-','*','/'};
		Calculator calculator1 = new Calculator();
		Calculator calculator2 = new Calculator();
		Scanner read = new Scanner(System.in);
		System.out.println("请输入要生成的题目数：");
		n = read.nextInt();
		read.nextLine();
		for(int i=1;i<=n;i++) {
			System.out.println("题目"+i+":");
		//	calculator.a=read.nextLine();
			a = rand.nextInt(100);
			b = rand.nextInt(100);
			c = rand.nextInt(100);
			calculator1.a = "("+a+""+ch[rand.nextInt(ch.length)]+ b+""+")"+ch[rand.nextInt(ch.length)]+c+"";
			calculator1.Getstack();
			System.out.print(calculator1.a+"=");
			r = calculator1.manage();
			calculator2.a=read.nextLine();
			calculator2.Getstack();
			answer = calculator2.manage();
			if(Rational.judge(r,answer)) {
				yes++;
				System.out.println("正确！");
			}
			else {
				if(r.denominator==1)
					System.out.println("错误，正确答案为："+r.numerator);
				else
					System.out.println("错误，正确答案为："+r.numerator+"/"+r.denominator);
			}
			
		}
		System.out.println("完成"+n+"道题目，正确率为"+(double)yes/n*100+"%");
	}
}

