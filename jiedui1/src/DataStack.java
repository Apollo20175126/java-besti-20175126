
public class DataStack {
	int n;
	Rational [] a;
	int top = -1;
	public DataStack(int m) {
		this.n=m;
	}
	void getStack() {
		a = new Rational[n];
		for(int i=0;i<n;i++) {
			a[i] = new Rational();
		}
	}
	void push(Rational data) {
		top++;
		a[top].numerator=data.numerator;
		a[top].denominator=data.denominator;
	}
	Rational pop() {
		return a[top--];
	}
}

