class A3 {
	final double PI = 3.1415926;
	public double getArea(final double r){
		return PI*r*r;
	}
	public final void speak() {
		System.out.println("您好，How's everything here?");
	}
}
public class EX5_9 {
	public static void main(String args[]) {
		A3 a = new A3();
		System.out.println("面积:"+a.getArea(100));
			a.speak();
	}
}
