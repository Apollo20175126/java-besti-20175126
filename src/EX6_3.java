abstract class MotorVehicles{
	abstract void brake();
}
interface MoneyFare{
	void charge();
}
interface ControlTemperature{
	void controlAirTemperature();
}
class Bus extends MotorVehicles implements MoneyFare {
	void brake() {
		System.out.println("公共汽车使用XX刹车技术");
	}
	public void charge() {
		System.out.println("公共汽车:一元/张，不计算公里数");
	}
}
class Taxi extends MotorVehicles implements MoneyFare, ControlTemperature {
        void brake() {
                System.out.println("出租车使用XX刹车技术");
        }
        public void charge() {
                System.out.println("出租车:两元/公里，起价3公里");
        }
	public void controlAirTemperature() {
		System.out.println("出租车安装了XX空调");
	}
}
class Cinema implements MoneyFare,ControlTemperature {
    public  void charge() {
        System.out.println("电影院:门票,十元/张");
    }
    public void controlAirTemperature() {
       System.out.println("电影院安装了中央空调");
    }
}
public class EX6_3 {
	public static void main(String args[]) {
		Bus bus101 = new Bus();
		Taxi buleTaxi = new Taxi();
		Cinema redStarCinema = new Cinema();
		MoneyFare fare;
		ControlTemperature temperature;
		fare = bus101;
		bus101.brake();
		fare.charge();
		fare = buleTaxi;
		temperature = buleTaxi;
		buleTaxi.brake();
		fare.charge();
		temperature = redStarCinema;
		fare = redStarCinema;
		temperature = redStarCinema;
		fare.charge();
		temperature.controlAirTemperature();
	}
}
